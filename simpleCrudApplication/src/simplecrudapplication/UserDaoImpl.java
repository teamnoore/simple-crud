/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package simplecrudapplication;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import simplecrudapplication.entities.Role;
import simplecrudapplication.entities.User;


public class UserDaoImpl implements UserDao {
private Connection connection = null; 
private String sql = null; 
private ResultSet resultSet = null ;
private Integer rows = 0 ;
private PreparedStatement statement = null ;

    public Connection setUpConnection() throws SQLException, ClassNotFoundException {
        
        SingletonDbConfig dbConfig = SingletonDbConfig.getInstance();
        return dbConfig.dbConnect();
    }

    
    public void closeConnection (Connection connection , PreparedStatement statement) throws SQLException
    {
        SingletonDbConfig dbConfig =  SingletonDbConfig.getInstance();
        dbConfig.closeConnection(connection, statement);
    }

    @Override
    public List<User> getAllUser() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateUser(User user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteUser() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User Search(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addUser(User user)  {

        try {
            connection = setUpConnection();

                sql = "insert into user(created,last_modified,emailaddress,firstname,lastname,password,username) values(?,?,?,?,?,?,?) ";

                statement = (PreparedStatement) connection.prepareStatement(sql, java.sql.Statement.RETURN_GENERATED_KEYS);
                statement.setDate(1, new java.sql.Date(user.getCreated().getTime()));
                statement.setDate(2, new java.sql.Date(user.getModified().getTime()));
                statement.setString(3, user.getEmail());
                statement.setString(4, user.getFirstName());
                statement.setString(5, user.getLastName());
                statement.setString(6, user.getPassword());
                statement.setString(7, user.getUsername());
                rows = statement.executeUpdate();
                resultSet = (java.sql.ResultSet) statement.getGeneratedKeys();
                resultSet.next();
            if (rows!=0) {
                for (Role role : user.getRoles()) {
                    sql = "insert into userroles(created,last_modified,role,userid) values(?,?,?,?) ";
                    statement = (PreparedStatement) connection.prepareStatement(sql, java.sql.Statement.RETURN_GENERATED_KEYS);
                    statement.setDate(1, new java.sql.Date(user.getCreated().getTime()));
                    statement.setDate(2, new java.sql.Date(user.getModified().getTime()));
                    statement.setString(3, role.getRoleName());
                    statement.setLong(4, resultSet.getInt(1));
                    statement.executeUpdate();
                    System.out.format("A new user with username %s  was added ", user.getUsername());
                }
            }
                else{
                    System.out.println("Unable to add new User");
                }



        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally{
            try {
                closeConnection(connection,statement);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
            
            
    }

}
