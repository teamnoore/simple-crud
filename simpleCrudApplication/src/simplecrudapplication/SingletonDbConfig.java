package simplecrudapplication;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class SingletonDbConfig {

    public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://localhost/useradminexample";
    public static final String USERNAME = "root";
    public static final String PASSWORD = "root";
    private static SingletonDbConfig instance;

    private SingletonDbConfig() {
        System.out.println("Singleton(): Initializing Instance");
    }

    public static SingletonDbConfig getInstance() {
        if (instance == null) {
            instance = new SingletonDbConfig();
        }
        return instance;
    }

    public Connection dbConnect() throws SQLException, ClassNotFoundException {
        Class.forName(JDBC_DRIVER);
        Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
        return conn;
    }

    public void closeConnection(Connection connection , Statement statement) throws SQLException {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
