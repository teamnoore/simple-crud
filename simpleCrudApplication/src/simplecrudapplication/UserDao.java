package simplecrudapplication;

import java.sql.SQLException;
import java.util.List;
import simplecrudapplication.entities.User;

public interface UserDao {

	public void addUser(User user) throws SQLException, ClassNotFoundException;
	
	public List<User> getAllUser ();
	
	public void updateUser(User user);
	
	public void deleteUser();
                
	
	public User Search (String id);
}