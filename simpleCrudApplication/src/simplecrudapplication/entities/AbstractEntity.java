/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package simplecrudapplication.entities;

import java.util.Date;

/**
 *
 * @author noorefatemah.suhabuth@castillelabs.com
 */
public abstract class AbstractEntity {

    private long id; 
    private Date created;
    private Date modified;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public AbstractEntity() {
    }
    
    
    
}
