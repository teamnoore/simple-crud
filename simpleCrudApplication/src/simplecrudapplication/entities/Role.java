/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package simplecrudapplication.entities;

/**
 *
 * @author noorefatemah.suhabuth@castillelabs.com
 */
public class Role extends AbstractEntity{
    
    private String roleName;
    private long user_id;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public Role() {
    }
    
    
    

    
}
