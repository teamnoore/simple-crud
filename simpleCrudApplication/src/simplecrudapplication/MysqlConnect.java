package simplecrudapplication;


import java.sql.Connection;
import java.sql.DriverManager;

public final class MysqlConnect {
	
    public Connection conn;
    public static MysqlConnect db;
    
    public final static MysqlConnect INSTANCE = new MysqlConnect();
    
    private MysqlConnect() {
    	
    }
    
    public Connection connect (){
    	String url= "jdbc:mysql://localhost:3306/";
        String dbName = "database_name";
        String driver = "com.mysql.jdbc.Driver";
        String userName = "root";
        String password = "rootmysql";
        try {
            Class.forName(driver).newInstance();
            this.conn = (Connection)DriverManager.getConnection(url+dbName,userName,password);
        }
        catch (Exception sqle) {
            sqle.printStackTrace();
        }
		return conn;
    };
 
}