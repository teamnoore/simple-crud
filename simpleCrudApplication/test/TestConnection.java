
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import simplecrudapplication.SingletonDbConfig;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author noorefatemah.suhabuth@castillelabs.com
 */
public class TestConnection {

    public static void main(String[] args) {
        SingletonDbConfig dbConfig = SingletonDbConfig.getInstance();
        Connection connection= null;
        try {
            connection = dbConfig.dbConnect();
           
        } catch (SQLException ex) {
            Logger.getLogger(TestConnection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    
    
    if(connection==null){
            System.out.println("Unable to test connection");

}
    else{
        System.out.println("Connection successfull");
    }
    }
    
    }
