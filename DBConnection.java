import java.sql.Connection;

public class SingletonDbConfig {
	
	private static SingletonDbConfig instance ;

	private Singleton()
	{
	System.out.println("Singleton(): Initializing Instance");
	}

	public static Singleton SingletonDbConfig()
	{
		if (instance == null)
		{
			synchronized(SingletonDbConfig.class)
			{
				if (instance == null)
				{
					System.out.println("getInstance(): First time getInstance was invoked!");
					instance = new SingletonDbConfig();
				}
			}            
		}

		return instance;
	}

	public Connection dbConnect()
	{
		//logic for db connection
	}

	public void closeConnection(){
		//logic for closing connection
	}
}