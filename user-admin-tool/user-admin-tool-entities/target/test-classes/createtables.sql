DROP TABLE IF EXISTS user;
CREATE TABLE user (
  id bigint(20)NOT NULL AUTO_INCREMENT,
  created datetime  ,
  last_modified datetime  ,
  emailaddress varchar(255),
  firstname varchar(255),
  lastname varchar(255),
  password varchar(255),
  username varchar(255) UNIQUE
);
DROP TABLE IF EXISTS user_role;
CREATE TABLE user_role (
  id bigint(20)NOT NULL AUTO_INCREMENT,
   created datetime  ,
  last_modified datetime  ,
  role varchar(255) ,
  user_id bigint(20) ,

);