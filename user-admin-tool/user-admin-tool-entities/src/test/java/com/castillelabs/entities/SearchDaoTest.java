//package com.castillelabs.entities;
//
//
//import com.castillelabs.entities.dao.UserDao;
//import com.castillelabs.entities.user.User;
//import org.junit.*;
//import static org.junit.Assert.assertEquals;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//
///**
// * @author noorefatemah.suhabuth@castillelabs.com on 28/06/2016.
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:/application-context-test.xml"})
//public class SearchDaoTest extends AbstractTransactionalJUnit4SpringContextTests {
//    @Autowired
//    private UserDao userDao;
//    @Before
//    public void setUp() {
//        executeSqlScript("createtables.sql", true);
//        executeSqlScript("insert.sql", true);
//        executeSqlScript("insertRole.sql", true);
//
//
//    }
//
//    @After
//    public void tearDown(){
//
//        executeSqlScript("deleteRole.sql", true);
//        executeSqlScript("delete.sql", true);
//
//    }
//    @Test
//    public void testSearch(){
//       User user = userDao.retrieveById(2l);
//       String username = "noore";
//       String lastname = "suhabuth";
//       String firstname = "noorefatemah";
//       String emailAddress = "noorefatemah.suhabuth@gmail.com";
//       String password ="password";
//       String role = "ROLE_EDITOR";
//       String role1 = "ROLE_REST";
//        assertEquals(username, user.getUsername());
//        assertEquals(lastname, user.getLastName());
//        assertEquals(firstname, user.getFirstName());
//        assertEquals(emailAddress, user.getEmailAddress());
//        assertEquals(password, user.getPassword());
//        assertEquals(role, user.getRoles().get(0).getRoleName());
//
//
//}
//    @Test
//    public void testSearchNotFound() {
//        User user = userDao.searchUserByUsername("john");
//        Assert.assertNull(user);
//
//    }
//
//    @Test
//    public void testSeachByUsername(){
//        User user = userDao.searchUserByUsername("noore");
//        String username = "noore";
//        String lastname = "suhabuth";
//        String firstname = "noorefatemah";
//        String emailAddress = "noorefatemah.suhabuth@gmail.com";
//        String password = "password";
//        String role = "ROLE_EDITOR";
//        String role1 = "ROLE_REST";
//        assertEquals(username, user.getUsername());
//        assertEquals(lastname, user.getLastName());
//        assertEquals(firstname, user.getFirstName());
//        assertEquals(emailAddress, user.getEmailAddress());
//        assertEquals(password, user.getPassword());
//        assertEquals(role, user.getRoles().get(0).getRoleName());
//
//
//    }
//
//}
