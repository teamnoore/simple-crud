//package com.castillelabs.entities;
//
//
//import com.castillelabs.entities.dao.UserDao;
//import com.castillelabs.entities.user.Role;
//import com.castillelabs.entities.user.User;
//import java.util.List;
//import org.junit.*;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
///**
// * @author noorefatemah.suhabuth@castillelabs.com on 28/06/2016.
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:/application-context-test.xml"})
//
//public class UpdateUserDaoTest extends AbstractTransactionalJUnit4SpringContextTests {
//
//    @Autowired
//    private UserDao userDao;
//    @Before
//    public void setUp(){
//        executeSqlScript("createtables.sql",true);
//        executeSqlScript("insert.sql", true);
//        executeSqlScript("insertRole.sql", true);
//
//
//    }
//    @After
//    public void tearDown(){
//        executeSqlScript("createtables.sql" ,false);
//        executeSqlScript("deleteRole.sql", true);
//        executeSqlScript("delete.sql", true);
//
//    }
//
//    @Test
//    public void testUpdateCheckPassword(){
//        User user = userDao.retrieveById(2l);
//        user.setPassword("hello");
//        userDao.saveOrUpdate(user);
//        User savedUser = userDao.retrieveById(2l);
//        Assert.assertEquals("hello" ,savedUser.getPassword());
//}
//    @Test
//    public void testUpdateCheckDetails(){
//    User user = userDao.retrieveById(2l);
//    user.setUsername("noorefatemah_suhabuth");
//        List<Role> roles = (List<Role>) user.getRoles();
//        roles.get(0).setRoleName("ROLE_SOAP");
//        userDao.saveOrUpdate(user);
//
//    List<User> users = userDao.retrieveAll();
//    Assert.assertEquals(user.getUsername(), users.get(0).getUsername());
//    Assert.assertEquals(((List<Role>) user.getRoles()).get(0).getRoleName(),users.get(0).getRoles().iterator().next().getRoleName());
//
//}
//
//
//}
