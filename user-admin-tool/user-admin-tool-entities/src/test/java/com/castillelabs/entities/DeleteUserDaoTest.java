//package com.castillelabs.entities;
//
//import com.castillelabs.entities.dao.UserDao;
//import com.castillelabs.entities.user.User;
//import java.util.List;
//import org.junit.*;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
///**
// * @author noorefatemah.suhabuth@castillelabs.com on 28/06/2016.
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:/application-context-test.xml"})
//
//public class DeleteUserDaoTest extends AbstractTransactionalJUnit4SpringContextTests {
//    @Autowired
//    private UserDao userDao;
//    @Before
//    public void setUp(){
//        executeSqlScript("createtables.sql",true);
//        executeSqlScript("insert.sql", true);
//        executeSqlScript("insertRole.sql", true);
//
//    }
//
//    @After
//    public void tearDown(){
//
//        executeSqlScript("deleteRole.sql", true);
//        executeSqlScript("delete.sql", true);
//
//    }
//
// @Test
//    public void testDelete(){
//        User user = userDao.retrieveById(2l);
//        userDao.delete(user);
//        List<User> userList = userDao.retrieveAll();
//        User user1 = userDao.retrieveById(2l);
//        Assert.assertEquals(null, user1);
//    }
//
//}
