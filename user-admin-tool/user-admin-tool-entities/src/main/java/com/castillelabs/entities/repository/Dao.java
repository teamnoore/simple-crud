package com.castillelabs.entities.repository;

import java.util.List;

/**
 * @author noorefatemah.suhabuth@castillelabs.com on 07/06/2016.
 */
public interface Dao<T> {

    public void persist(T object);

    public T saveOrUpdate(T object);

    public T retrieveById(Long id);

    public List<T> retrieveAll();

    public boolean delete(T object);

   

}
