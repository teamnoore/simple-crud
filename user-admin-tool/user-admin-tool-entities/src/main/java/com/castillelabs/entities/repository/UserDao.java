package com.castillelabs.entities.repository;

import com.castillelabs.entities.user.User;
import java.util.List;

/**
 * @author noorefatemah.suhabuth@castillelabs.com on 01/06/2016.
 */
public interface UserDao extends Dao<User> {
    public User searchUserByUsername(String username);

    public boolean isUserExist(String username);
    public List<User> searchByRole(String roleName);
   public List<User> searchByLastName (String lastName);
   public List<User> searchByFirstName (String firstName);
   public List<User> searchUserListByUsername (String username);
 

}
