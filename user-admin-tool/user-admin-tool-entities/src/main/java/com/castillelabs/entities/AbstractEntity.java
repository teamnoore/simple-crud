package com.castillelabs.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @author noorefatemah.suhabuth@castillelabs.com on 26/05/2016.
 */
@MappedSuperclass
@XmlRootElement
@XmlAccessorType(value = XmlAccessType.FIELD)
public abstract class AbstractEntity implements Serializable {

    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XmlTransient
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created")
    @XmlTransient
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_modified")
    @XmlTransient
    private Date lastModified;

    public Date getCreated() {
        return created;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getLastModified() {
        return lastModified;
    }

    @PreUpdate
    public void setLastModified() {
        this.lastModified = new Date();
    }

    @PrePersist
    public void setCreated() {
        this.created = new Date();
        this.lastModified = new Date();
    }
}
