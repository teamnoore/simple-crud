package com.castillelabs.entities.user;

import com.castillelabs.entities.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.*;

/**
 * Created by noorefatemah Suhabuth on 26/05/2016.
 */

@XmlRootElement
@Entity
@Table(name="user")
@XmlAccessorType(value = XmlAccessType.FIELD)
public  class User extends AbstractEntity implements Serializable {

    @Column(name="firstname")
    @NotNull(message="Please enter a firstname! ")
    @Size(min=3 , message = "Please enter atleast 3 characters!")
    @XmlAttribute( required = true)
    private String firstName;
    @Column(name = "lastname")
    @NotNull(message="Please enter a lastname")
    @Size(min = 3 , message = "Please enter at least 3 characters!")
    @XmlAttribute( required = true)
    private String lastName;
    @Column(name = "username", unique = true)
    @NotNull(message="Please enter username")
    @XmlAttribute( required = true)
    private String username;
    @NotNull(message = "Please enter a password!")
    @XmlAttribute( required = true)
    private String password;
    @Column(name = "emailaddress")
    @NotNull(message = "Please enter an email Address")
    @Pattern(regexp = ".+@.+\\..+",message="Email Address should be in the correct format ")
    @XmlAttribute( required = true)
    private String emailAddress;
    @OneToMany(targetEntity = Role.class,mappedBy = "user", cascade = {CascadeType.PERSIST,CascadeType.REMOVE,CascadeType.REFRESH},fetch = FetchType.EAGER)
    @JsonManagedReference
    @XmlElement(required =true)
    private List<Role> roles ;
    private transient boolean saved;

    public User(/*default contstructor for user*/) {}

    public User(String firstName, String lastName, String username, String password, String emailAddress, List<Role> roles, boolean saved) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.emailAddress = emailAddress;
        this.roles = roles;
        this.saved = saved;
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }


   
    public String getFirstName() {
        return firstName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setFirstName(String firstName) {
       this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

   

}
