/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jms.example;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBException;

/**
 *
 * @author noorefatemah.suhabuth@castillelabs.com
 */
public class MessageProducer extends App {

    public static void main(String[] args) throws JMSException, JAXBException {

        Connection connection = connectionFactory.createConnection();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Queue queue = session.createQueue("nooreTestQueue");
        javax.jms.MessageProducer messageProducer = session.createProducer(queue);
        for (int i = 0; i < 100; i++) {
            
        
        MessageRequest messageRequest = new MessageRequest("Hello this is the message producer","Some other things related to message");
        TextMessage textMessage = session.createTextMessage();
        String xml = marshall(messageRequest);
        textMessage.setText(xml);
        textMessage.setJMSCorrelationID("noore-123-ID");
        System.out.println("JMS QUEUE : " + queue.getQueueName());
        System.out.println("sending message");
        messageProducer.send(textMessage);
        }
        
        messageProducer.close();
        session.close();
        connection.close();

    }
}
