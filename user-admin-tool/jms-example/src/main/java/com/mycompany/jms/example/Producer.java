/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jms.example;

import java.io.StringWriter;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.xml.transform.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

/**
 *
 * @author noorefatemah.suhabuth@castillelabs.com
 */
public class Producer {
    @Autowired
    private JmsTemplate jmsTemplate ;
    
    @Autowired
    private Jaxb2Marshaller jaxb2Marshaller;
    
    public void sendMessage(){
         MessageRequest messageRequest = new MessageRequest("Producer Message", "id-999");
         
        jmsTemplate.send("nooreTestQueue",(Session session) -> {
               TextMessage textMessage = session.createTextMessage();
            for (int i = 0; i < 100; i++) {
                StringWriter stringWriter = new StringWriter();
                jaxb2Marshaller.marshal(messageRequest, (Result) stringWriter);
             
                textMessage.setText(stringWriter.toString());
                textMessage.setJMSCorrelationID("id :  "+ i);
               
            }
            return textMessage;
        });
    }

}
