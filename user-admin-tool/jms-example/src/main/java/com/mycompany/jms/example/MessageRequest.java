/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jms.example;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author noorefatemah.suhabuth@castillelabs.com
 */
@XmlRootElement
public class MessageRequest implements Serializable{
    
    private String message ; 
    private String somethingRelatedToMessage; 

    public MessageRequest(String message, String somethingRelatedToMessage) {
        this.message = message;
        this.somethingRelatedToMessage = somethingRelatedToMessage;
    }

    public MessageRequest() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSomethingRelatedToMessage() {
        return somethingRelatedToMessage;
    }

    public void setSomethingRelatedToMessage(String somethingRelatedToMessage) {
        this.somethingRelatedToMessage = somethingRelatedToMessage;
    }

    @Override
    public String toString() {
        return "MessageRequest{" + "message=" + message + ", somethingRelatedToMessage=" + somethingRelatedToMessage + '}';
    }
    

}
