/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jms.example;

import com.rabbitmq.jms.admin.RMQConnectionFactory;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.ConnectionFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author noorefatemah.suhabuth@castillelabs.comó
 */
public abstract class App {
static ConnectionFactory connectionFactory;
private static JAXBContext jAXBContext ;
    static {
    try {
        jAXBContext = JAXBContext.newInstance(MessageRequest.class);
    } catch (JAXBException ex) {
        Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
    }
        RMQConnectionFactory rMQConnectionFactory = new RMQConnectionFactory();
        rMQConnectionFactory.setHost("127.0.0.1");
        rMQConnectionFactory.setPort(5672);
//        connectionFactory = rMQConnectionFactory;
     
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL("tcp://127.0.0.1:61616");
        connectionFactory = activeMQConnectionFactory;
    }
    
    public static String marshall(Object object) throws JAXBException{
        Marshaller marshaller =  jAXBContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        StringWriter stringWriter = new StringWriter();
         marshaller.marshal(object, stringWriter);
       String xml = stringWriter.toString(); 
       return xml;
    }
    
    public static String unMarshall (Object object) throws JAXBException{
        Unmarshaller unmarshaller = jAXBContext.createUnmarshaller(); 
        unmarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        StringReader stringReader = new StringReader((String) object);
        unmarshaller.unmarshal(stringReader);
        String result = stringReader.toString();
        return result;
    }
    
}
