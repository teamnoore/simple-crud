
package com.mycompany.useradmintoolclient.stubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mycompany.useradmintoolclient.stubs package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AbstractEntity_QNAME = new QName("http://service.api.castillelabs.com/", "abstractEntity");
    private final static QName _Role_QNAME = new QName("http://service.api.castillelabs.com/", "role");
    private final static QName _User_QNAME = new QName("http://service.api.castillelabs.com/", "user");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mycompany.useradmintoolclient.stubs
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Role }
     * 
     */
    public Role createRole() {
        return new Role();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link UserArray }
     * 
     */
    public UserArray createUserArray() {
        return new UserArray();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AbstractEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.api.castillelabs.com/", name = "abstractEntity")
    public JAXBElement<AbstractEntity> createAbstractEntity(AbstractEntity value) {
        return new JAXBElement<AbstractEntity>(_AbstractEntity_QNAME, AbstractEntity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Role }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.api.castillelabs.com/", name = "role")
    public JAXBElement<Role> createRole(Role value) {
        return new JAXBElement<Role>(_Role_QNAME, Role.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link User }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.api.castillelabs.com/", name = "user")
    public JAXBElement<User> createUser(User value) {
        return new JAXBElement<User>(_User_QNAME, User.class, null, value);
    }

}
