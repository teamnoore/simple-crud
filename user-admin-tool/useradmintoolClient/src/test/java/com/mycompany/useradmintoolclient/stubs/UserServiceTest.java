/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.useradmintoolclient.stubs;

import java.util.Random;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

/**
 *
 * @author root
 */
@ContextConfiguration(classes = {UserServiceTest.class})
@Configuration
@ImportResource({"classpath:application-context.xml"})
public class UserServiceTest extends AbstractTestNGSpringContextTests {
    
    static final Logger log = LoggerFactory.getLogger(UserServiceTest.class);

    @Autowired(required = true)
    @Qualifier("userService")
    private UserService userService;
    
    double sd = 0.9;
    double mean = 500;
    
    Random random = new Random();
    
    //@BeforeMethod
    public void before() throws InterruptedException{
        long sleep = (long) (random.nextGaussian() * mean * sd);
        sleep = Math.abs(sleep);
        Thread.sleep(sleep);
    }

    @Test(threadPoolSize = 10, invocationCount = 500)
    public void testSomeMethod() {
        long startMilis = System.currentTimeMillis();
        
        User user = userService.searchUserById(2);
        ToStringBuilder builder = new ReflectionToStringBuilder(user);
        log.debug("{}", builder.toString());
        
        long endMilis = System.currentTimeMillis();
        log.info("Time Taken: {}ms", endMilis-startMilis);
        
    }
    
    @Test(threadPoolSize = 10 , invocationCount = 100) 
    public void testRetrieveByUsername(){
        log.info("Testing retrieve by username");
      long startMilis = System.currentTimeMillis();
      
        User user = userService.searchUserByUsername("noor8s");
        ToStringBuilder builder = new ReflectionToStringBuilder(user);
        log.debug("{}", builder.toString());
        long endMilis = System.currentTimeMillis();
        log.info("Time Taken: {}ms", endMilis-startMilis);
    }
    
    
    @Test(threadPoolSize = 10 , invocationCount = 100)
    public void addUser(){
        
    }

}
