/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.useradmintoolclient.stubs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author noorefatemah.suhabuth@castillelabs.com
 */
@ContextConfiguration(classes = {UserServiceAddUserTest.class})
@Configuration
@ImportResource({"classpath:application-context.xml"})
public class UserServiceAddUserTest extends AbstractTestNGSpringContextTests {
    
    static final Logger log = LoggerFactory.getLogger(UserServiceAddUserTest.class);
    @Autowired(required = true)
    @Qualifier("userService")
    private UserService userService;
    
    List<User> userList;
    
    @BeforeClass
    public void loadusers() throws FileNotFoundException, IOException {
        userList = new ArrayList<>();
        
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("user.csv").getFile());
        
        Reader input = new FileReader(file);
        Iterable<CSVRecord> records = CSVFormat.DEFAULT.withHeader().parse(input);
        for (CSVRecord record : records) {
            User user = new User();
            Role role = new Role();
            user.setFirstName(record.get("firstname"));
            user.setLastName(record.get("lastname"));
            
            user.setPassword(record.get("password"));
            user.setEmailAddress(record.get("emailaddress"));
            String[] roles = record.get("role").split(";");
            for (String roleCsv : roles) {
                role.setRoleName(roleCsv);
                user.getRoles().add(role);
            }
            userList.add(user);
        }
        
        log.debug("loadusers() = {} loaded", userList.size());
    }
    
    AtomicInteger i = new AtomicInteger(0);
    
    private User getUser() {
        
        int size = userList.size();
        
        int current = i.getAndAccumulate(1, (existing, add) -> {
            return (existing + add) % size;
        });
        
        return userList.get(current);
        
    }
    
    @Test(threadPoolSize = 10, invocationCount = 500, timeOut = 100000)
    public void loadDataFromCsv() throws FileNotFoundException, IOException {
        
        User user = getUser();
        
        int length = 10;
        boolean useLetters = true;
        boolean useNumbers = false;
        String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
        user.setUsername(generatedString);
        
        long startMilis = System.currentTimeMillis();
        try {
            userService.addUser(user);
            ToStringBuilder builder = new ReflectionToStringBuilder(user);
            log.debug("{}", builder.toString());
        } finally {
            long endMilis = System.currentTimeMillis();
            log.info("Time Taken: {}ms", endMilis - startMilis);
        }
        
    }
}
