///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package user.service.config;
//
//import javax.sql.DataSource;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//
///**
// *
// * @author Robert
// */
//@Configuration
//@EnableWebSecurity
//public class SecurityConfig extends WebSecurityConfigurerAdapter {
//
//    @Autowired
//    private DataSource dataSource;
//
//    @Bean
//    public ShaPasswordEncoder shaPasswordEncoder() {
//        ShaPasswordEncoder shaPasswordEncoder = new ShaPasswordEncoder(256);
//
//        return new ShaPasswordEncoder(256);
//    }
//
//    @Autowired
//    protected void configureManager(AuthenticationManagerBuilder auth) throws Exception { //NOSONAR
//        auth.inMemoryAuthentication().withUser("admin").password("admin").roles("ADMIN");
//        auth.jdbcAuthentication().dataSource(dataSource)
//                .passwordEncoder(shaPasswordEncoder())
//                .usersByUsernameQuery("SELECT username, password, 'true' enabled FROM username where username=?")
//                .authoritiesByUsernameQuery("SELECT username, userrole FROM username_role where username=?");
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception { //NOSONAR
//        http.authorizeRequests()
//                .anyRequest().fullyAuthenticated().and()
//                .httpBasic().and()
//                .csrf().disable();
//    }
//
//}
