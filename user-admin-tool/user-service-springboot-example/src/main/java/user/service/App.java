/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 *
 * @author Robert
 */
@SpringBootApplication
public class App { //NOSONAR

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(App.class, args); //NOSONAR
        context.registerShutdownHook();
    }

}
