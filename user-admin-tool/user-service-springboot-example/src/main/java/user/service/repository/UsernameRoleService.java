/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.service.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import user.service.model.UsernameRole;

/**
 *
 * @author Robert
 */
public interface UsernameRoleService extends PagingAndSortingRepository<UsernameRole, Long> {

    List<UsernameRole> findByUsername(String username);

    @Query("select distinct ur.userrole from UsernameRole as ur")
    List<String> findRoles();
}
