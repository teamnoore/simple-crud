<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en-US">
    <head>

        <title>User Admin tool</title>
        <%@ include file ="../../include/lib.jsp" %>
        <link href="<c:url value='../../../resources/css/app.css' />" rel="stylesheet"/>
        <style>
            label {
                width: 2000px;
            }
            input[type='text'] {
                display: inline;
                width: 600px;
            }
            input[type='password'] {
                display: inline;
                width: 600px;
            }

            legend a {
                color: inherit;
            }
            legend.legendStyle {
                padding-left: 5px;
                padding-right: 5px;
            }
            fieldset.fsStyle {
                font-family: Verdana, Arial, sans-serif;
                font-size: small;
                font-weight: normal;
                border: 1px solid #999999;
                padding: 4px;
                margin: 5px;
            }
            legend.legendStyle {
                font-size: 90%;
                color: #888888;
                background-color: transparent;
                font-weight: bold;
            }

            legend {
                width: auto;
                border-bottom: 0px;
            }

        </style>

    </head>
    <body ng-app="myApp" class="ng-cloak">
        <div class="generic-container" ng-controller="UserController">
            <div class="panel panel-default">
                <div class="panel-heading"><span class="lead"style="align-content: center" id="heading">Add New User</span></div>

                <div class="formcontainer">
                    <form ng-submit="submit()" name="myForm" class="form-horizontal">
                        <input type="hidden" ng-model="user.id" />
                        <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
                        <div class="row">
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">Username</span>
                                            <input id="username" name="prependedtext" class="form-control"
                                                   placeholder="Enter Username" type="text" required="" ng-model="user.username">
                                        </div>

                                    </div>

                                    <div class="has-error">
                                        <span ng-show="usernamenNull" id="usernamenNull">User Name cannot be null</span>
                                        <span ng-show="usernameMinLen" id = "usernameMinLen">Minimum length required is 3</span>
                                        <span ng-show="usernameExist" id="usernameExist">The username already exist </span>
                                    </div>
                                </div>

                                <!-- Prepended text-->
                                <div class="form-group">
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">First Name </span>
                                            <input id="firstName" name="prependedtext" class="form-control"
                                                   placeholder="Enter First Name" type="text" ng-model="user.firstName">
                                        </div>
                                    </div>

                                    <div class="has-error">
                                        <span ng-show="firstnameNull">This is a required field</span>
                                        <span ng-show="firstnameMinLen">Minimum length required is 3</span>
                                    </div>
                                </div>
                     

                        <div class="form-group">
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon">Last Name </span>
                                    <input id="prependedtext" name="prependedtext" class="form-control"
                                           placeholder="Enter Last Name" type="text" ng-model="user.lastName">
                                </div>
                                <div class="has-error">
                                    <span ng-show="lastNameNull">This is a required field</span>
                                    <span ng-show="lastNameMinLen">Minimum length required is 3</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon">Password</span>
                                    <input id="password" name="password" class="form-control"
                                           placeholder="Enter Password" type="password" ng-model="user.password">
                                </div>
                            </div>
                            <div class="has-error" >
                                <span ng-show="passwordNull">This is a required field</span>

                            </div>

                        </div>

                        <!-- Prepended text-->
                        <div class="form-group">
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon">Confirm Password</span>
                                    <input id="confirmPassword" name="confirmPassword" class="form-control"
                                           placeholder="Re enter password" type="password">
                                </div>
                            </div>
                            
                            <div class="has-error" >
                                <span ng-show="confirmPasswordNull">This is a required field</span>
                                <span ng-show="passwordMatch">The passwords does not match</span>
                        </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon">Email Address</span>
                                    <input id="prependedtext" name="prependedtext" class="form-control"
                                           placeholder="Enter email address" type="text" ng-model="user.emailAddress">
                                </div>
                            </div>
                            <div class="has-error">
                                <span ng-show="emailNull">This is a required field</span>
                                <span ng-show="emailFormat">The format of the email address is invalid!</span>
                            </div>
                            
                        </div>

                        <!-- Select Multiple -->
                        <div class="form-group">

                            <div class="col-md-8">
                              <select name="multipleSelect" id="multipleSelect" ng-model="data.multipleSelect" multiple ng-init="user.roles.role">
                                    <option value="ROLE_EDITOR">ROLE_EDITOR</option>
                                    <option value="ROLE_REST">ROLE_REST</option>
                                    <option value="ROLE_SOAP">ROLE_SOAP</option>
                                    <option value="ROLE_ALL">ROLE_ALL</option>
                                </select><br>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-8">
                                <input type="submit" value="{{!user.id ? 'Add' : 'Update'}}"
                                       class="btn btn-primary btn-sm">
                                <button type="button" ng-click="reset()" class="btn btn-warning btn-sm">Reset Form
                                </button>

                            </div>
                        </div>

                        </fieldset>

                </div>




                <div class="panel-footer">
                    <div class="row">
                        <div>
                            <p class="text-center text-success" id="message">
                                <strong color="green">
                                </strong>
                            </p>
                        </div>
                    </div>
                </div>
                <div>    

                    <div class="formcontainer col-md-12" >
                        <fieldset class='fsStyle'>
                            <legend class='legendStyle'>
                                <a data-toggle="collapse" data-target="#demo" href="#">Search User(Click to input search criteria to search for user!) </a>
                            </legend>
                            <div class="row collapse " id="demo">

                                <div class="form-group col-md-12">
                                    <label class="col-md-2">Search By : </label>
                                    <div class="col-md-7">
                                        <select  ng-model="data.searchParameter">
                                            <option value="searchCriteria">Please Select Search criteria</option>
                                            <option value="Firstname">Firstname</option>
                                            <option value="Lastname">Lastname</option>
                                            <option value="Username">Username</option>
                                            <option value="Role">Role</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-md-12">

                                    <label class="col-md-2">Search Text : </label>  
                                    <div class="col-md-7">
                                        <input type="text" ng-keyup="search(data.searchParameter, searchText)" ng-model="searchText" id="txtSearchText" 
                                               placeholder="Search Text"  ng-model-options="{ updateOn: 'default blur', debounce: { 'default': 400, 'blur': 1} }"/> 
                                    </div>
                                    <!--                  <div class='col-md-2'>
                                                      <input type="submit" value="GO" ng-click="search(data.searchParameter,searchText)"class="btn btn-primary btn-sm" >
                                                      </div>-->
                                </div>



                            </div>

                    </div>
                    </fieldset>
                </div>

            </div>
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading"><span class="lead">List of Users </span></div>
                <div class="ng-table-container">
                    <%--<label for="items">items by page</label>--%>
                    <%--<input class="input-sm form-control ng-valid ng-touched ng-dirty ng-valid-number" name="items" id="items" type="number" ng-model="itemsByPage"/>--%>
                    <table ng-table ="usersTable" class="table table-striped">
                        <tr ng-repeat="user in $data">
                            <td title="'First name'"  sortable="'firstName'">
                                {{user.firstName}}</td>
                            <td title="'Last Name'"  sortable="'lastName'">
                                {{user.lastName}}</td>
                            <td title="'Username'" sortable="'username'">
                                {{user.username}}</td>
                            <td title="'Email Address'"  sortable="'emailAddress'">
                                {{user.emailAddress}}</td>
                            <td title="'Role'"><span ng-repeat = "roles in user.roles">
                                    {{roles.roleName}}</span>
                            <td title="'Operations'">
                                <button type="button" ng-click="edit(user.id)" class="btn btn-success custom-width">Edit</button>
                                <button type="button" ng-click="remove(user.id)" class="btn btn-danger custom-width">Remove</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
    <script src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>
    <link rel="stylesheet" href="https://rawgit.com/esvit/ng-table/master/dist/ng-table.min.css">
    <script src="https://rawgit.com/esvit/ng-table/master/dist/ng-table.min.js"></script>
    <script src="<c:url value='../../../resources/js/app.js' />"></script>
    <script src="<c:url value='../../../resources/js/user_service.js' />"></script>

    <script src="<c:url value='../../../resources/js/user_controller.js' />"></script>


</body>
</html>