myApp.factory('UserService', ['$http', '$q', function($http, $q){

    return {
        fetchAllUsers: function() {
            return $http.get('http://localhost:8080/user/')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        
                        return $q.reject(errResponse);
                    }
                );
        },
        getUserByUsername: function() {
            return $http.get('http://localhost:8080/user/'+username)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                      
                        return $q.reject(errResponse);
                    }
                );
        },

       isExistUser: function(username) {
            return $http.get('http://localhost:8080/user/IsUserExist/'+username)
                .then(
                    function(response){
                       
                        return false;

                    },
                    function(errResponse){
                        document.getElementById("usernameExist").style.visibility = "show";
                        var status = errResponse.status;
                       return true;
                    }
                );
        },

        createUser: function(user){
            return $http.post('http://localhost:8080/createUser', user)
                .then(
                    function(response){
                        var status = response.status;
                        return status;
                    },
                    function(errResponse){
                        var status = errResponse.status;
                        return status;
                        // return $q.reject(errResponse);
                    }
                );
        },

        updateUser: function(user, id){
            return $http.put('http://localhost:8080/user/'+id, user)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                       
                        return $q.reject(errResponse);
                    }
                );
        },

        deleteUser: function(id){
            return $http.delete('http://localhost:8080/user/'+id)
                .then(
                    function(response){
                     
                        return response.data;
                    },
                    function(errResponse){
                      
                        return $q.reject(errResponse);
                    }
                );
        }

    };

}]);