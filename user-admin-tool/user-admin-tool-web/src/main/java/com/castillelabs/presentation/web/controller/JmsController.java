/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castillelabs.presentation.web.controller;

import com.castillelabs.api.request.EmailRequest;
import com.castillelabs.presentation.web.jmsService.JmsService;
import com.castillelabs.presentation.web.mailService.EmailHtmlSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.context.Context;

/**
 *
 * @author Noorefatemah Suhabut
 */
@RestController
public class JmsController {
   @Autowired
   private JmsService jmsService;
   
   @Autowired
  private EmailHtmlSender emailHtmlSender;
                  
           
    @RequestMapping(value = "/sendToQueue")
    public void sendToQueue (@RequestBody EmailRequest request){

        jmsService.sendMessage(request);
    }
//     @RequestMapping(value="/receive")
//	public void receive(){
//		 jmsService.receiveMessage();
//	}
 
@RequestMapping(value="/sendEmail")
	public void send(){
		    Context context = new Context();
        context.setVariable("Title","Title");
         boolean sent =  emailHtmlSender.send("noorefatemah.suhabuth@castillelabs.com", "type", "template", context);
	}
 
}
