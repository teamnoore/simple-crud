package com.castillelabs.presentation.web.config;

import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.Marshaller;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MarshallingMessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;


/**
 * @author noorefatemah.suhabuth@castillelabs.com on 09/09/2016.
 */
@Configuration
@EnableJms
public class JmsConfig {

private static final String DEFAULT_BROKER_URL = "tcp://localhost:61616";
private static final String QUEUE_NAME ="mailQueue";

@Autowired
private JmsTemplate jmsTemplate;
@Bean
public ActiveMQConnectionFactory connectionFactory(){
    ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
    connectionFactory.setBrokerURL(DEFAULT_BROKER_URL);
    
    return connectionFactory;
}

  @Bean
    public JmsTemplate jmsTemplate(){
        JmsTemplate template = new JmsTemplate();
        template.setConnectionFactory(connectionFactory());
        template.setDefaultDestinationName(QUEUE_NAME);
        template.setMessageConverter(messageConverter());
        
               
        return template;
    }

   @Bean
    public MessageConverter messageConverter() {
    MarshallingMessageConverter converter = new MarshallingMessageConverter();
    converter.setMarshaller(marshaller());
    converter.setUnmarshaller(marshaller());
   
    return converter;
    }
    private Jaxb2Marshaller marshaller() {
    Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
     marshaller.setPackagesToScan("com.castillelabs.api.request");
    return marshaller;
}
    
   




}
