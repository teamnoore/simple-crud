/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castillelabs.presentation.web.mailService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

/**
 *
 * @author root
 */
@Component
public class EmailHtmlSender {
    @Autowired
    private MailSenderService mailSenderService ; 
    
    @Autowired
    private SpringTemplateEngine templateEngine ;
    
   public boolean send(String to, String subject, String templateName, Context context) {
        String body = templateEngine.process(templateName, context);
        return mailSenderService.sendHtml(to, subject, body);
    }
}
