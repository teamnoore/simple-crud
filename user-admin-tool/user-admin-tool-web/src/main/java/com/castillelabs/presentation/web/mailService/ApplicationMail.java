///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.castillelabs.presentation.web.mailService;
//
//import com.castillelabs.presentation.web.jms.Main;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.context.annotation.ComponentScan;
//
///**
// *
// * @author Noorefatemah Suhabut
// */
//@SpringBootApplication
//@ComponentScan(basePackages = {"com.castillelabs.presentation.web.mailService"})
//public class ApplicationMail implements CommandLineRunner{
//    @Autowired
//    private MailSenderService mailSenderService; 
//
//    @Override
//    public void run(String... args) throws Exception {
//
//    mailSenderService.sendEmail();
//    }
//    
//    public static void main(String[] args) {
//        SpringApplication.run(ApplicationMail.class, args);
//    }
//           
//           
//}
