/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castillelabs.presentation.web.jmsService;

import com.castillelabs.api.request.EmailRequest;
import com.castillelabs.api.service.UserService;
import com.castillelabs.entities.user.User;
import com.castillelabs.presentation.web.mailService.EmailHtmlSender;

import java.util.Calendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

/**
 *
 * @author Noorefatemah Suhabut
 */
@Service
public class JmsServiceImpl implements JmsService{

    @Autowired
    private JmsTemplate jmsTemplate;
    @Autowired
    private EmailHtmlSender emailHtmlSender;
 private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(JmsServiceImpl.class);
    @Autowired
    UserService userService;
    @Override
    public void sendMessage(EmailRequest emailRequest) {
        
    jmsTemplate.convertAndSend(emailRequest);
}

    @Override
     @JmsListener(destination = "mailQueue")
    public void receiveMessage(EmailRequest emailRequest) {
        User user = userService.searchUserByUsername(emailRequest.getUsername());
        Context context = new Context();
        context.setVariable("date", Calendar.getInstance());
        String title = "" ;
      if (null!=emailRequest.getEmailTemplate())
          switch (emailRequest.getEmailTemplate()) {
            case FORGOT_PASSWORD_EMAIL:
                title ="Password Reset Request";
                context.setVariable("title", title);
                context.setVariable("username", user.getUsername());
                context.setVariable("url", "http://localhost:8080/resetPassword/"+user.getUsername());
                break;
            case UPDATED_DETAILS:
                title="Updated Details";
                context.setVariable("title", title);
                context.setVariable("username", user.getUsername());
                context.setVariable("url", "http://localhost:8080/home");
                break;
            case REGISTRATION_EMAIL:
                title="Registration";
                context.setVariable("title", title);
                context.setVariable("username", user.getUsername());
                break;
            default:
                break;
        }
        boolean sent =  emailHtmlSender.send(user.getEmailAddress(), title, "template", context);
        LOGGER.info(String.valueOf(sent));
  
        
    }
}

    
    

