/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castillelabs.presentation.web.controller;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author noorefatemah.suhabuth@castillelabs.com
 */
@ComponentScan(basePackages = {"com.castillelabs.service","com.castillelabs.presentation.web"})
@EnableJpaRepositories(basePackages={"com.castillelabs.entities.dao"})
@EntityScan(basePackages={"com.castillelabs.entities"})
@ImportResource ({"classpath:/spring/cxf-context.xml","classpath:/spring/spring-security-context.xml"})
@EnableWebMvc
@SpringBootApplication
public class Application  {


	public static void main(final String[] args) {
		SpringApplication.run(Application.class, args);
	}


}