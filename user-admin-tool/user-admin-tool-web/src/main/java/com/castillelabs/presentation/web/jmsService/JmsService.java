/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castillelabs.presentation.web.jmsService;

import com.castillelabs.api.request.EmailRequest;

/**
 *
 * @author Noorefatemah Suhabut
 */
public interface JmsService {
    public void sendMessage(EmailRequest emailRequest);
    public void receiveMessage(EmailRequest emailRequest);
    
}
