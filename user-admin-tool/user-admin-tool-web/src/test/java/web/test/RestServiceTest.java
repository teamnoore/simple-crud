//package web.test;
//
//import com.castillelabs.api.service.UserService;
//import com.castillelabs.entities.user.Role;
//import com.castillelabs.entities.user.User;
//import com.castillelabs.presentation.web.rest.UserRestServices;
//import java.util.ArrayList;
//import java.util.List;
//import org.hamcrest.Matchers;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import static org.mockito.Matchers.eq;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.runners.MockitoJUnitRunner;
//import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
//
///**
// * @author noorefatemah.suhabuth@castillelabs.com on 25/07/2016.
// */
//@RunWith(MockitoJUnitRunner.class)
//public class RestServiceTest {
//        @InjectMocks
//        UserRestServices userRestService;
//        @Mock
//        UserService userService;
//        @Mock
//        ShaPasswordEncoder shaPasswordEncoder;
//
//        private User user;
//        private List<User> userList = new ArrayList<User>();
//        private List<Role> roleList = new ArrayList<Role>();
//
//        @Before
//        public void setUp() {
//
//            Role role = new Role();
//            role.setRoleName("ROLE_REST");
//            roleList.add(role);
//            user = new User("noore", "suhabuth", "noor", "password", "noore@gmail,com", roleList, false);
//            userList.add(user);
//            setUpMocks();
//
//        }
//        @After
//    public void tearDown() {
//        user = null;
//        userList = null;
//        roleList = null;
//
//    }
//
//        public void setUpMocks() {
//            Mockito.when(userService.retrieveAllUser()).thenReturn(userList);
//            Mockito.when(userService.searchUserByUsername(eq("noor"))).thenReturn(userList.stream().findFirst().get());
//            Mockito.when(userService.searchUserById(eq(1l))).thenReturn(userList.stream().findFirst().get());
////            Mockito.when(userService.updateUser(new User())).thenReturn(null);
//
//
//
//        }
//        @Test
//        public void getUsers() {
//            List<User> userLists = userRestService.getAllUsers();
//            Assert.assertThat(userLists.stream().findFirst().get().getFirstName(), Matchers.is(userList.stream().findFirst().get().getFirstName()));
//            Assert.assertThat(userLists.stream().findFirst().get().getLastName(), Matchers.is(userList.stream().findFirst().get().getLastName()));
//            Assert.assertThat(userLists.stream().findFirst().get().getUsername(), Matchers.is(userList.stream().findFirst().get().getUsername()));
//            Assert.assertThat(userLists.stream().findFirst().get().getEmailAddress(), Matchers.is(userList.stream().findFirst().get().getEmailAddress()));
//            Assert.assertThat(userLists.stream().findFirst().get().getRoles().get(0).getRoleName(), Matchers.is(userList.stream().findFirst().get().getRoles().get(0).getRoleName()));
//            Assert.assertThat(userLists.stream().findFirst().get().getPassword(), Matchers.is(userList.stream().findFirst().get().getPassword()));
//
//        }
//
//        @Test
//        public void getUsersByUsername() {
//            User retrivedUser = userRestService.searchByUsername("noor");
//            Assert.assertThat(retrivedUser.getFirstName(), Matchers.is(this.userList.stream().findFirst().get().getFirstName()));
//            Assert.assertThat(retrivedUser.getLastName(), Matchers.is(this.userList.stream().findFirst().get().getLastName()));
//            Assert.assertThat(retrivedUser.getUsername(), Matchers.is(this.userList.stream().findFirst().get().getUsername()));
//            Assert.assertThat(retrivedUser.getEmailAddress(), Matchers.is(this.userList.stream().findFirst().get().getEmailAddress()));
//            Assert.assertThat(retrivedUser.getRoles().get(0).getRoleName(), Matchers.is(this.userList.stream().findFirst().get().getRoles().get(0).getRoleName()));
//            Assert.assertThat(retrivedUser.getPassword(), Matchers.is(this.userList.stream().findFirst().get().getPassword()));
//        }
//    @Test
//    public void getUserById() {
//        User retrivedUser = userRestService.searchById(1l);
//        Assert.assertThat(retrivedUser.getFirstName(), Matchers.is(this.userList.stream().findFirst().get().getFirstName()));
//        Assert.assertThat(retrivedUser.getLastName(), Matchers.is(this.userList.stream().findFirst().get().getLastName()));
//        Assert.assertThat(retrivedUser.getUsername(), Matchers.is(this.userList.stream().findFirst().get().getUsername()));
//        Assert.assertThat(retrivedUser.getEmailAddress(), Matchers.is(this.userList.stream().findFirst().get().getEmailAddress()));
//        Assert.assertThat(retrivedUser.getRoles().get(0).getRoleName(), Matchers.is(this.userList.stream().findFirst().get().getRoles().get(0).getRoleName()));
//        Assert.assertThat(retrivedUser.getPassword(), Matchers.is(this.userList.stream().findFirst().get().getPassword()));
//    }
//        @Test
//        public void addUser() {
//            Role role = new Role();
//            role.setRoleName("ROLE_REST");
//            roleList.add(role);
//            User newUser = new User("noore", "suhabuth", "noorefatemah_S", "password", "noore@gmail,com", roleList, false);
//            Mockito.when(userService.addUser(eq(newUser))).thenReturn(newUser);
//            User saveUser = userRestService.createUser(newUser);
//            boolean added = true;
//            Assert.assertThat(added, Matchers.is(saveUser.isSaved()));
//            Assert.assertThat(newUser, Matchers.is(saveUser));
//        }
//
//      @Test
//        public void updateUserTest() {
//            Role role = new Role();
//            role.setRoleName("ROLE_EDITOR");
//            roleList.add(role);
//            User newUser = new User("noorefatemah", "Suhabuth", "noore1", "oasssss", "noore0792@live.com", roleList, false);
//             Mockito.when(userService.updateUser(eq(newUser))).thenReturn(newUser);
//              User saveUser = userRestService.updateUser(newUser);
//              boolean added = true;
//                Assert.assertThat(newUser, Matchers.is(saveUser));
//
//        }
//
//    @Test
//    public void deleteUserTest() {
//        user = userRestService.searchById(1l);
//        user.setId(1l);
//        userRestService.deleteUser(user.getId());
//        Mockito.when(userService.delete(user.getId())).thenReturn(userList.remove(user));
//        Assert.assertEquals(0, userList.size());
//    }
//
//    }
//
//
