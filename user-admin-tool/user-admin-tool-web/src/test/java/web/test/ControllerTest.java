///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package web.test;
//
//import com.castillelabs.presentation.web.controller.UserController;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.MockitoAnnotations;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.ResultActions;
//import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
//import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;
//
///**
// *
// * @author noorefatemah.suhabuth@castillelabs.com
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:/spring/applicationContext.xml",
//    "classpath:/spring/persistenceContext.xml","classpath:/spring/spring-security-servlet.xml"})
//public class ControllerTest {
//    private MockMvc mockMvc;
//    @InjectMocks
//    private UserController userController;
//    @Before
//    public void setUp() {
//        MockitoAnnotations.initMocks(this);
//        mockMvc = standaloneSetup(userController).build();
//    }
//
//
//    @Test
//    public void testUserPage() throws Exception {
//
//        MockHttpServletRequestBuilder getRequest = get("/home").accept(MediaType.ALL);
//        ResultActions results = mockMvc.perform(getRequest);
//        results.andExpect(status().isOk());
//        results.andExpect(view().name("user"));
//
//    }
//
//    @Test
//    public void testLoginPage() throws Exception {
//        MockHttpServletRequestBuilder getRequest = get("/signin").accept(MediaType.ALL);
//        ResultActions results = mockMvc.perform(getRequest);
//        results.andExpect(status().isOk());
//        results.andExpect(view().name("logiN"));
//
//    }
//
//
//}
