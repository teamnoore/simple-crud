///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package com.castillelabs.service;
//
//import com.castillelabs.entities.dao.UserDao;
//import com.castillelabs.entities.user.Role;
//import com.castillelabs.entities.user.User;
//import com.castillelabs.service.service.UserServiceImpl;
//import java.util.ArrayList;
//import java.util.List;
//import org.hamcrest.Matchers;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import static org.mockito.Matchers.any;
//import static org.mockito.Matchers.eq;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.runners.MockitoJUnitRunner;
//import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
//
///**
// *
// * @author noorefatemah.suhabuth@castillelabs.com
// */
//@RunWith(MockitoJUnitRunner.class)
//public class UserServiceTest {
//    @InjectMocks
////    UserServiceImpl userService;
//    @Mock
//    UserDao userDao;
//    @Mock
//    ShaPasswordEncoder shaPasswordEncoder;
//
//    private User user;
//    private List<User> userList=new ArrayList<User>();
//    private List<Role> roleList = new ArrayList<Role>();
//
//    @Before
//    public void setUp(){
//
//        Role role = new Role();
//        role.setRoleName("ROLE_REST");
//        roleList.add(role);
//         user = new User("noore", "suhabuth", "noor",shaPasswordEncoder.encodePassword("password", "hello"), "noore@gmail,com", roleList,false);
//         userList.add(user);
//        setUpMocks();
//
//    }
//
//    @After
//    public void tearDown() {
//        user =null;
//        userList = null;
//       roleList = null;
//
//    }
//
//    public void setUpMocks(){
//        Mockito.when(userDao.retrieveAll()).thenReturn(userList);
//        Mockito.when(shaPasswordEncoder.encodePassword(eq("password"),any())).thenReturn("password encoded");
//        Mockito.when(userDao.searchUserByUsername(eq("noor"))).thenReturn(userList.stream().findFirst().get());
//        Mockito.when(userDao.isUserExist(eq("noore_07"))).thenReturn(true);
//        Mockito.when(userDao.retrieveById(eq(1l))).thenReturn(userList.stream().findFirst().get());
//
//
//    }
//
//    @Test
//    public void getUsers(){
//        List<User>userLists = userService.retrieveAllUser();
//
//        Assert.assertThat(userLists.stream().findFirst().get().getFirstName(), Matchers.is(userList.stream().findFirst().get().getFirstName()));
//        Assert.assertThat(userLists.stream().findFirst().get().getLastName(), Matchers.is(userList.stream().findFirst().get().getLastName()));
//        Assert.assertThat(userLists.stream().findFirst().get().getUsername(), Matchers.is(userList.stream().findFirst().get().getUsername()));
//        Assert.assertThat(userLists.stream().findFirst().get().getEmailAddress(), Matchers.is(userList.stream().findFirst().get().getEmailAddress()));
//        Assert.assertThat(userLists.stream().findFirst().get().getRoles().get(0).getRoleName(), Matchers.is(userList.stream().findFirst().get().getRoles().get(0).getRoleName()));
//        Assert.assertThat(userLists.stream().findFirst().get().getPassword(), Matchers.is(userList.stream().findFirst().get().getPassword()));
//    }
//
//    @Test
//    public void getUsersByUsername() {
//        User retrivedUser = userService.searchUserByUsername("noor");
//
//
//
//        Assert.assertThat(retrivedUser.getFirstName(), Matchers.is(this.userList.stream().findFirst().get().getFirstName()));
//        Assert.assertThat(retrivedUser.getLastName(), Matchers.is(this.userList.stream().findFirst().get().getLastName()));
//        Assert.assertThat(retrivedUser.getUsername(), Matchers.is(this.userList.stream().findFirst().get().getUsername()));
//        Assert.assertThat(retrivedUser.getEmailAddress(), Matchers.is(this.userList.stream().findFirst().get().getEmailAddress()));
//        Assert.assertThat(retrivedUser.getRoles().get(0).getRoleName(), Matchers.is(this.userList.stream().findFirst().get().getRoles().get(0).getRoleName()));
//        Assert.assertThat(retrivedUser.getPassword(), Matchers.is(this.userList.stream().findFirst().get().getPassword()));
//    }
//    @Test
//    public void getUserById() {
//        User retrivedUser = userService.searchUserById(1l);
//        Assert.assertThat(retrivedUser.getFirstName(), Matchers.is(this.userList.stream().findFirst().get().getFirstName()));
//        Assert.assertThat(retrivedUser.getLastName(), Matchers.is(this.userList.stream().findFirst().get().getLastName()));
//        Assert.assertThat(retrivedUser.getUsername(), Matchers.is(this.userList.stream().findFirst().get().getUsername()));
//        Assert.assertThat(retrivedUser.getEmailAddress(), Matchers.is(this.userList.stream().findFirst().get().getEmailAddress()));
//        Assert.assertThat(retrivedUser.getRoles().get(0).getRoleName(), Matchers.is(this.userList.stream().findFirst().get().getRoles().get(0).getRoleName()));
//        Assert.assertThat(retrivedUser.getPassword(), Matchers.is(this.userList.stream().findFirst().get().getPassword()));
//    }
//
//    @Test
//    public void userDoesNotExist(){
//        Role role = new Role();
//        role.setRoleName("ROLE_REST");
//        roleList.add(role);
//        user = new User("noore", "suhabuth", "noore", shaPasswordEncoder.encodePassword("password", "hello"), "noore@gmail,com", roleList,false);
//        boolean exists = userService.isUserExist(user.getUsername());
//        Assert.assertFalse(exists);
//
//    }
//    @Test
//    public void userExists() {
//        Role role = new Role();
//        role.setRoleName("ROLE_REST");
//        roleList.add(role);
//        user = new User("noore", "suhabuth", "noore_07", shaPasswordEncoder.encodePassword("password", "hello"), "noore@gmail,com", roleList, false);
//        boolean exists = userService.isUserExist(user.getUsername());
//        Assert.assertTrue(exists);
//
//    }
//    @Test
//    public void addUser() {
//        Role role = new Role();
//        role.setRoleName("ROLE_REST");
//        roleList.add(role);
//        user = new User("noore", "suhabuth", "noorefatemah_S", shaPasswordEncoder.encodePassword("password", "hello"), "noore@gmail,com", roleList,false);
//        User saveUser = userService.addUser(user);
//        boolean added = true;
//        Assert.assertThat(added, Matchers.is(saveUser.isSaved()));
//        Assert.assertThat(user, Matchers.is(saveUser));
//    }
//
//    @Test
//    public void addExistingUserTest() {
//        Role role = new Role();
//        role.setRoleName("ROLE_REST");
//        roleList.add(role);
//        user = new User("noore", "suhabuth", "noore_07", shaPasswordEncoder.encodePassword("password", "hello"), "noore@gmail,com", roleList , false);
//        boolean added = false;
//        User addedUser = userService.addUser(user);
//        Assert.assertThat(added, Matchers.is(addedUser.isSaved()));
//    }
//
//    @Test
//    public void testGeneratePassword(){
//        String encodedPassword = shaPasswordEncoder.encodePassword("password", "hello");
//        Assert.assertThat(encodedPassword, Matchers.is("password encoded"));
//    }
//
//   @Test
//   public void updateUserTest(){
//      Role role = new Role();
//      role.setRoleName("ROLE_EDITOR");
//      roleList.add(role);
//          user = new User("noorefatemah", "Suhabuth", "noore1", "oasssss", "noore0792@live.com", roleList , false);
//            User savedUser = userService.updateUser(user);
//            Assert.assertThat(user, Matchers.is(savedUser));
//   }
//
//    @Test
//    public void deleteUserTest() {
//        user = userService.searchUserById(1l);
//        user.setId(1l);
//        userService.delete(user.getId());
//         Mockito.when(userDao.delete(user)).thenReturn(userList.remove(user));
////        userService.delete(user.getId());
//        Assert.assertEquals(0,userList.size());
//    }
//}
