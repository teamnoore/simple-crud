package com.castillelabs.web.spring.boot.web.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.SpringServletContainerInitializer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @author noorefatemah.suhabuth@castillelabs.com on 08/09/2016.
 */
@ComponentScan(basePackages = {"com.castillelabs.service","com.castillelabs.web"})
@EnableJpaRepositories(basePackages={"com.castillelabs.repository"})
@EntityScan(basePackages={"com.castillelabs.model"})
@SpringBootApplication
public class SampleWebJspApplication extends SpringServletContainerInitializer {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(SampleWebJspApplication.class, args);
    }
}
