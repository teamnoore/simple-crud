/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castillelabs.api.spring.boot.api;

import com.castillelabs.model.spring.boot.model.account.Account;

/**
 *
 * @author noorefatemah.suhabuth@castillelabs.com
 */
public interface AccountService
{
public Account findOne(String number) throws AccountNotFoundException; 
public Account createAccountByNumber(String number);
}
