/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castillelabs.api.spring.boot.api;

/**
 *
 * @author noorefatemah.suhabuth@castillelabs.com
 */
public class AccountNotFoundException extends RuntimeException{
    public AccountNotFoundException(String accountId) {
        super("No such account with id: " + accountId);
    }
}
