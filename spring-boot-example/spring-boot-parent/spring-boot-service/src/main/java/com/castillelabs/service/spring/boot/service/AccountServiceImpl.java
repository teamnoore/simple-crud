/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castillelabs.service.spring.boot.service;

import com.castillelabs.api.spring.boot.api.AccountNotFoundException;
import com.castillelabs.api.spring.boot.api.AccountService;
import com.castillelabs.model.spring.boot.model.account.Account;
import com.castillelabs.repository.spring.boot.repository.repository.AccountRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.stereotype.Service;

/**
 *
 * @author noorefatemah.suhabuth@castillelabs.com
 */

@Service("accountService")
public class AccountServiceImpl implements AccountService{
@Value("${dummy.type}")
private String dummyType;

@Autowired
private AccountRepository accountRepository;
    @Override
    public Account findOne(String number) throws AccountNotFoundException {
       if(number.equals("0000")) {
            throw new AccountNotFoundException("0000");
        }
        
        Account account = accountRepository.findByNumber(number);
        if(account == null){
          account = createAccountByNumber(number);
        }

        return account;
    }

    @Override
    public Account createAccountByNumber(String number) {
      Account account = new Account();
        account.setNumber(number);
        return accountRepository.save(account);
    }
    
    
   public String getDummyType()
   {
       return dummyType;
   }
    public void setDummyType(String dummyType){
        this.dummyType = dummyType;
    }
    
}
